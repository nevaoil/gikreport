# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.6...v0.0.7) (2021-10-03)

### [0.0.6](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.5...v0.0.6) (2021-10-03)

### [0.0.5](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.4...v0.0.5) (2021-10-03)


### Features

* Добавлен Dockerfile и скрипт деплоя серверной части ([faf1760](https://bitbucket.org/nevaoil/gikreport/commit/faf176046126d84485c56295a671423bede1e83e))
* Добавлен каркас серверного приложения ([9641af3](https://bitbucket.org/nevaoil/gikreport/commit/9641af3596b1b3b6bcbc646579c76a9b611c4f99))
* Добавлена страница для входа ([5c8dbc9](https://bitbucket.org/nevaoil/gikreport/commit/5c8dbc9e048cce915a9944d9f3ec434f44b0ae0d))
* Добавлено проксирование клиентских запросов к серверному api ([5e4db14](https://bitbucket.org/nevaoil/gikreport/commit/5e4db147809f100a3d61d27a3abee12b8a1fae7e))


### Bug Fixes

* Исправлено определение глобальных sass переменных ([ebae06f](https://bitbucket.org/nevaoil/gikreport/commit/ebae06f40c634b612c9132d966dd86c689b8aa81))

### [0.0.4](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.3...v0.0.4) (2021-10-02)


### Features

* Добавлено оповещение о выпуске версии/успешном деплое в телеграм ([4e71f75](https://bitbucket.org/nevaoil/gikreport/commit/4e71f7590ea3f3b3fbe31232b016e9a6406a2a7f))

### [0.0.3](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.2...v0.0.3) (2021-10-02)

### [0.0.2](https://bitbucket.org/nevaoil/gikreport/compare/v0.0.1...v0.0.2) (2021-10-02)


### Features

* Удалена конфигурация деплоя из ветки development ([12c3b37](https://bitbucket.org/nevaoil/gikreport/commit/12c3b372f5e8016a0dc962148ab2232ea28ca2db))

### 0.0.1 (2021-10-02)


### Features

* initial files ([ed02a61](https://bitbucket.org/nevaoil/gikreport/commit/ed02a61612aa795693eb58045a73920394089df9))
* Добавлена конфигурация деплоя приложения ([c86191c](https://bitbucket.org/nevaoil/gikreport/commit/c86191c115f209e2fe02976d20a1616ca3c9029e))
