import axios from 'axios'

export default
{
  getStatus () {
    return axios.get('/api/app/state')
      .then(() => 200)
      .catch(err => err.status)
  }
}
