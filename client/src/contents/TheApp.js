import AppService from '@/services/AppService'
import TheLoadingContent from '@/contents/TheLoadingContent'

export default {
  name: 'App',

  data () {
    return {
      appStatus: null
    }
  },

  async beforeCreate () {
    this.appStatus = await AppService.getStatus()
  },

  render (h) {
    if (this.appStatus === null) {
      return h(TheLoadingContent)
    }
    return h('router-view')
  }
}
