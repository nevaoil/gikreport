import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, _, next) => {
  if (to.meta.title) {
		document.title = `${to.meta.title} | GIK Report`;
	} else {
		document.title = 'GIK Report';
  }
  next();
});

export default router
