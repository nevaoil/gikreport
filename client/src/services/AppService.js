import app from '@/api/app'

export default
{
  async getStatus () {
    return await app.getStatus()
  }
}
