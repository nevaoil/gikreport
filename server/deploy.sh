#!/bin/bash
docker stop gikreport 
docker rm gikreport
docker rmi gikreport
docker load --input $REMOTE_PATH_SERVER/gikreport.tar.gz
docker run --name gikreport --restart unless-stopped -p $REMOTE_SERVER_PORT:80 -d gikreport