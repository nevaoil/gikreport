const path = require('path')
global.appRoot = path.resolve(__dirname);
require('module-alias/register')
const config = require('@/config')
const server = require('@/server')

server.init(config.port)
