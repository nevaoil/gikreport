const config = require('@/config.json')

module.exports = Object.assign(
  {
    base: '/api',
    port: 80,
    isDevelopment: process.env.NODE_ENV !== 'production'
  },
  config,
  process.env
)
