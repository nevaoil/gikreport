
module.exports = function (err, req, res, next) {
  if (typeof (err) === 'string') {
    return res.status(400).json({ error: err })
  }

  if (err.name === 'UnauthorizedError') {
    return res.status(401).json({ code: 401, error: 'Invalid Token', response: null })
  }
  
  return res.status(500).json({ code: 500,  error: err.message, response: null })
}
