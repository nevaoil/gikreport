const express = require('express')
const http = require('http')
const logger = require('@/utils/logger')(module)

const app = express()

app.use(express.json())

require('@/utils/load.controllers')(app)

app.use(require('@/middleware/MethodNotAllowedMiddleware'))
app.use(require('@/middleware/ErrorHandlerMiddleware'))

module.exports.init = function (port) {
  return new Promise(resolve => {
    const httpServer = http.createServer(app)
    
    httpServer.listen(port, () => {
        logger.debug(`Server listen ${port} port.`)

        resolve(httpServer)
    })
  })
}
