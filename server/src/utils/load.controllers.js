const fs = require('fs')
const logger = require('@/utils/logger')(module)
const config = require('@/config')

const controllerDirectory = global.appRoot + '/controllers/'

module.exports = function (app) {
  fs.readdirSync(controllerDirectory)
    .forEach(v => {
      const match = v.match(/([A-Z]\w+)(Controller.js)$/);

      if (match != null) {
        const snakeCase = match[1]
          .replace(/[A-Z]/g, v => '-' + v.toLowerCase())
          .substring(1);
        const module = require(controllerDirectory + v);

        if (typeof module === 'function' && module.name === 'router') {
          logger.debug('Register routes: ' + config.base + '/' + snakeCase);
          app.use(config.base + '/' + snakeCase, module);
        } else {
          logger.error(`Incorrect controller module: ${v}`);
        }
      }
    })
}