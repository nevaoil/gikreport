const winston = require('winston')
const config = require('@/config')

const logger = new winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  exitOnError: false
})

if (config.isDevelopment) {
  logger.add(new winston.transports.Console({
    level: 'debug',
    format: winston.format.simple()
  }))
}

module.exports = function (context) {
  const module = context.filename
    .replace(/^.*[\\\/]/, '')
    .replace(/.js$/, '')
  
  return {
    error: (...args) => logger.error(...args, { module }),
    warn: (...args) => logger.warn(...args, { module }),
    info: (...args) => logger.info(...args, { module }),
    debug: (...args) => logger.debug(...args, { module })
  }
}
